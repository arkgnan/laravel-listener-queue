<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notifikasi</title>
</head>
<body>
    <p>Selamat Saudara/i {{ $nama }} , blog Anda yang berjudul {{ $judul }}, akan kami review.<br/>
    Kami akan mengirimkan email kepada Anda jika blog Anda sudah berhasil dipublish</p>
</body>
</html>