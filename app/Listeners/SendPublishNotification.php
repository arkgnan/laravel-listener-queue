<?php

namespace App\Listeners;

use App\Events\PublishNotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\PublishNotification;
use Illuminate\Support\Facades\Mail;

class SendPublishNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogNotificationEvent  $event
     * @return void
     */
    public function handle(PublishNotificationEvent $event)
    {
        Mail::to($event->blog->user->email)->send(new PublishNotification());
    }
}
