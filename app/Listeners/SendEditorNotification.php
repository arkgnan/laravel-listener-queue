<?php

namespace App\Listeners;

use App\Events\BlogNotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\EditorNotification;
use Illuminate\Support\Facades\Mail;

class SendEditorNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogNotificationEvent  $event
     * @return void
     */
    public function handle(BlogNotificationEvent $event)
    {
        Mail::to('editor@example.com')->send(new EditorNotification());
    }
}
