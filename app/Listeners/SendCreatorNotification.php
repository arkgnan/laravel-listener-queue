<?php

namespace App\Listeners;

use App\Events\BlogNotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\BlogNotification;
use Illuminate\Support\Facades\Mail;

class SendCreatorNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogNotificationEvent  $event
     * @return void
     */
    public function handle(BlogNotificationEvent $event)
    {
        Mail::to($event->blog->user->email)->send(new BlogNotification($event->blog));
    }
}
