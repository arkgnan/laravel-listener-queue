<?php

namespace App\Http\Controllers;

use App\Events\BlogNotificationEvent;
use App\Http\Requests\BlogRequest;
use App\Models\Blogs;
use Illuminate\Http\Request;
use App\Events\PublishNotificationEvent;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = Blogs::create([
            "judul" => request('judul'),
            "user_id" => request('user')
        ]);
        
        event(new BlogNotificationEvent($blog));

        return $blog;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blogs::find($id);
        
        if(request('status')==1){
            $status = 'Status Blog berhasil di publish';
        } else {
            $status = 'Blog telah dimasukkan ke draft';
        }

        $blog->update([
            "publish_status" => request('status')
        ]);
        
        event(new PublishNotificationEvent($blog));
        
        return response()->json(['message'=>$status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
