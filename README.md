Sanbercode Pekan 3 tugas 2 : Event Listener & Queue  

**create blog** 
post `api/blog/create`

```json
{
    "judul": "artikel 1",
    "user": 1
}
```

**update status publish**  
patch `api/blog/update/{id}`

```json
{
    "status":1
}
```